import React, {createContext, useState, useRef} from 'react';

const ProfileContext = createContext();

const ProfileProvider = ({children}) => {
  const [profileContextData, setProfileContextData] = useState('');
  const [range, setRange] = useState(10)
  const [userLocation, setuserLocation] = useState({})
  const [isLoaded, setLoaded] = useState("")
  const[zipCode, setZipCode] = useState("")
  const mapRef = useRef();
  return (
    <ProfileContext.Provider
      value={{
        profileContextData,
        setProfileContextData,
        range,
        setRange, 
        userLocation,
        setuserLocation,
        isLoaded,
        setLoaded,
        mapRef,
        zipCode, 
        setZipCode
      }}>
      {children}
    </ProfileContext.Provider>
  );
};

export {ProfileContext, ProfileProvider};