import React from 'react'

const Budget = () => {
    const toggleCarousel = (action) => {
        const { Carousel } = require("bootstrap");
        const carousel = new Carousel("#myCarousel");
        if (action === "next") {
          carousel.next();
        } else {
          carousel.prev();
        }
      };

  return (
    <div className="inp_wrap">
        <div className="ringslider">
            <p><strong>Budget</strong></p>

            <div className="def_ringprice">
                <span>$1,000</span>
                <span>$1,0000</span>
            </div>

            <div className="ringslider_cont">
                <div className="slider-parent">
                    <input type="range" min="1" max="500"/>
                </div>
            </div>

            <h4>$3,000</h4>
            <p  className="otpinfo">Estimated household reach based on budget</p>

        </div>
        <div className="checkoption_view">
            <div className="checkoption_left">
                Audience type select all that apply
            </div>
            <div className="checkoption_right">
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id="flexResidential"/>
                    <label className="form-check-label" htmlFor="flexResidential" checked>Residential/ Consumer</label>
                </div>

                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id="flexBusiness"/>
                    <label className="form-check-label" htmlFor="flexBusiness">Business</label>
                </div>

                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id="flexPOBox"/>
                    <label className="form-check-label" htmlFor="flexPOBox">PO Box</label>
                </div>

            </div>

        </div>

        <div className="defbutton_opt btnoptview">
            <button type="button" className="defbutton prevbtn"  onClick={() => toggleCarousel("prev")}>Previous</button>
            <button type="button" className="defbutton"  onClick={() => toggleCarousel("next")} >Next</button>
        </div>
  </div> 
  )
}

export default Budget