import React from 'react'

const Demographics = () => {
  return (
    <>
    <div  className="radio_option" >
            <div className="radio_info" >
                    <input type="radio" id="test3" name="radio-group2"  />
                    <label for="test3">Radius (miles)</label>
            </div>
            <div className="radio_info" >
                <input type="radio" id="test4" name="radio-group2" />
                <label for="test4">Drive-time (minutes)</label>
            </div>
    </div>
    <div className="inp_wrap">
        <div className="ringslider">
            <p><strong>Household income range</strong></p>
                <div className="def_ringprice">
                <span>$25k</span>
                <span>$250k+</span>
                </div>
            <div className="ringslider_cont">
                <div className="slider-parent">
                    <input type="range" min="1" max="15"/>
                </div>

            </div>
            <p><strong>Median Age Range</strong></p>
                <div className="def_ringprice">
                <span>18 Years</span>
                <span>65+ Years</span>
                </div>
            <div className="ringslider_cont">
                <div className="slider-parent">
                    <input type="range" min="1" max="15"/>
                </div>

            </div>
        </div>
        <div className="checkoption_view">
            <div className="checkoption_left">
            Audience type select all that apply
            </div>
            <div className="checkoption_right">
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id="flexResidential"/>
                    <label className="form-check-label" for="flexResidential" checked>
                    Residential/ Consumer
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id="flexBusiness"/>
                    <label className="form-check-label" for="flexBusiness">
                    Business
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id="flexPOBox"/>
                    <label className="form-check-label" for="flexPOBox">
                    PO Box
                    </label>
                </div>
            </div>
        </div>
    
        {/* <div className="defbutton_opt btnoptview">
            <button type="button" className="defbutton prevbtn"  >Previous</button>
        <button type="button" className="defbutton" data-bs-target="#changeLocation">Next</button>
        </div> */}
    </div> 
    </>       
  )
}

export default Demographics 