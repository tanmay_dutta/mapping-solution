import React from "react";
import { useState, useMemo, useRef, useCallback, useContext } from "react";
import { GoogleMap, Marker, Circle, Polygon, DrawingManager} from "@react-google-maps/api";
import Placedetail from "./Placedetail";
import Zipcode from "./Zipcode";
import styles from "../styles/area.module.css";
import { useEffect } from "react";
import { useLoadScript } from "@react-google-maps/api";
import Loader from "./Loader";
import { ProfileContext } from "../Service/ProfileProvider";
import axios from "axios";

const Map = ({ coordinates, setCoordinates }) => {
  const [userCenter, setuserCenter] = useState({
    lat: 40.63463151377654,
    lng: -97.89969605983609,
  });
  const [coordinate, setCoordinate] = useState({
    lat: 37.78825,
    lng: -122.4324,
  });
  const [pollyPaths, setpollyPaths] = useState([]);
  const [isoLine, setisoLine] = useState([]);
  const [data, setData] = useState(null);
  const { range, setuserLocation, mapRef, zipCode, userLocation } =
    useContext(ProfileContext);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        var mapLocation = { lat: latitude, lng: longitude };

        if (mapLocation != coordinates) {
          setCoordinate(mapLocation);
        } else {
          setCoordinate(coordinates);
        }

        if (zipCode.length === 0) {
          console.log("zipcode is empty");
        } else {
          console.log("here is zip code", zipCode);
          allZipcodes();
          setCoordinate(userLocation)
          getHomeOwnershipData()
        }

        if(userLocation && userLocation != "undifined"){
          isoLinePolygon();
        }else{
          console.log("user location empty")
        }
      }
    );

  }, [zipCode, userLocation]);

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
    libraries: ["places","drawing"],
  });

  const options = useMemo(
    () => ({
      disableDefaultUI: true,
      clickableIcons: false,
    }),
    []
  );
  const onMapLoad = useCallback((map) => {
    mapRef.current = map;
  }, []);

  const panTo = useCallback(({ lat, lng }) => {
    mapRef.current.panTo({ lat, lng });
    setCoordinate({ lat, lng });
    setuserLocation({ lat, lng });
  }, []);

  const allZipcodes = async () => {
    let newArr = [];
    await axios
      .get(
        `https://nodeserver.mydevfactory.com:3311/user/getAllZipcodeData?zip_code=${zipCode}`,
        {
          headers: {},
        }
      )
      .then(async (response) => {
        setpollyPaths(newArr);

        for (let i = 0; i < response.data.data.length; i++) {
          if (response.data.data[i].geometry.type === "Polygon") {
            newArr.push(
              response.data.data[i].geometry.coordinates[0].map((item) => ({
                lat: item[1],
                lng: item[0],
              }))
            );
          } else {
            let temp = [];
            temp = response.data.data[i].geometry.coordinates[0];
            for (let j = 0; j < temp.length; j++) {
              newArr.push(
                temp[j].map((item) => ({ lat: item[1], lng: item[0] }))
              );
            }
          }
        }
      })
      .catch(function (error) {
        if (error.response) {
          console.log("response error===>", error.response);
        } else if (error.request) {
          console.log("Request Error==>", error.request);
        } else {
          console.log("Error occured", error.message);
        }
      });
  };

  const isoLinePolygon = async () => {
    console.log("user location ==> ",userLocation)
    await axios
      .get(
        `https://nodeserver.mydevfactory.com:3311/user/getIsoLine?latitude=${userLocation.lat}&longitude=${userLocation.lng}&type=time&mode=drive&range=900`,
        {
          headers: {},
        }
      )
      .then(async (response) => {
        setisoLine(
          response.data.data[0].geometry.coordinates[0][0].map((item) => ({
            lat: item[1],
            lng: item[0],
          }))
        );
      })
      .catch(function (error) {
        if (error.response) {
          console.log("response error===>", error.response);
        } else if (error.request) {
          console.log("Request Error==>", error.request);
        } else {
          console.log("Error occured", error.message);
        }
      });
  };

  const getHomeOwnershipData =  async () => {
    const API_KEY = 'a4f8f62ce9f27aaed22c6fefa8bfeb085e1fba5b';
    await axios
      .get(
        // `https://api.census.gov/data/2019/acs/acs5?get=NAME,B25003_001E&for=zip%20code%20tabulation%20area:*&key=${API_KEY}`,
        // `https://api.census.gov/data/2021/acs/acs5/subject?get=NAME,group(S0101)&for=us:1&key=${API_KEY}`,
        // `https://api.census.gov/data/2021/acs/acs5/profile?get=NAME,group(DP04)&for=us:1&key=${API_KEY}`,
        // `https://api.census.gov/data/2021/pep/population?get=DENSITY_2021,POP_2021,NAME,STATE&for=region:*&key=${API_KEY}`,
        // `https://api.census.gov/data/2018/acs/acs5?get=B19013_001E,B01002_001E,B25003_001E,B25003_002E&for=zip%20code%20tabulation%20area:${zipCode}&key=${API_KEY}`,
        // `https://api.census.gov/data/2018/acs/acs5?get=B19013_001E,B01002_001E,B25003_001E,B25003_002E&for=zip%20code%20tabulation%20area:12345&key=${API_KEY}`,
        `https://geocoding.geo.census.gov/geocoder/geographies/coordinates?x=-73.935242&y=40.730610&benchmark=Public_AR_Current&vintage=Current_Current&layers=14&format=json`,
      
      )
      .then(async (response) => {
       console.log("population response", response)
      })
      .catch(function (error) {
        if (error.response) {
          console.log("response error===>", error.response);
        } else if (error.request) {
          console.log("Request Error==>", error.request);
        } else {
          console.log("Error occured", error.message);
        }
      });
   
  }

  const paths = pollyPaths;

  const Poptions = {
    fillColor: "#FFFFFF",
    fillOpacity: 0.5,
    strokeColor: "#000000",
    strokeOpacity: 1,
    strokeWeight: 1,
    clickable: false,
    draggable: false,
    editable: false,
    geodesic: false,
    zIndex: 1,
  };

  const isoLinePaths = isoLine;

  const isoptions = {
    fillColor: "red",
    fillOpacity: 0.2,
    strokeColor: "#000000",
    strokeOpacity: 1,
    strokeWeight: 3,
    clickable: false,
    draggable: false,
    editable: false,
    geodesic: false,
    zIndex: 1,
  };

  const drawingOptions = {
    fillColor: "red",
    fillOpacity: 1,
    strokeColor: "green",
    strokeOpacity: 1,
    strokeWeight: 3,
    clickable: false,
    draggable: true,
    editable: false,
    geodesic: false,
    zIndex: 1,
  };


  // const drawingManager = new google.maps.drawing.DrawingManager({
  //   drawingMode: google.maps.drawing.OverlayType.MARKER,
  //   drawingControl: true,
  //   drawingControlOptions: {
  //     position: google.maps.ControlPosition.TOP_CENTER,
  //     drawingModes: [
  //       google.maps.drawing.OverlayType.MARKER,
  //       google.maps.drawing.OverlayType.CIRCLE,
  //       google.maps.drawing.OverlayType.POLYGON,
  //       google.maps.drawing.OverlayType.POLYLINE,
  //       google.maps.drawing.OverlayType.RECTANGLE,
  //     ],
  //   },
  //   markerOptions: {
  //     icon: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
  //   },
  //   circleOptions: {
  //     fillColor: "#ffff00",
  //     fillOpacity: 1,
  //     strokeWeight: 5,
  //     clickable: false,
  //     editable: true,
  //     zIndex: 1,
  //   },
  // });

  const PonLoad = (polygon) => {
    console.log("polygon: ", polygon);
  };

  const onDrawLoad = drawingManager => {
    console.log("drawing manager",drawingManager)
  }
  
  const onDrawPolygonComplete = polygon => {
    console.log("pollygon drawing",polygon)

  }

 

  if (!isLoaded)
    return (
      <>
        <Loader />
      </>
    );

  return (
    <div className={styles.mapView}>
      <Placedetail panTo={panTo} />
      <Zipcode panTo={panTo} />

      <GoogleMap
        zoom={8}
        center={userCenter}
        mapContainerClassName={styles.mapView}
        options={options}
        onLoad={onMapLoad}
      >
        <Marker position={coordinate} />

        <Circle
          center={coordinate}
          radius={1609.34*5* range}
          options={middleOptions}
        />
        <DrawingManager
        onLoad={onDrawLoad}
        onPolygonComplete={onDrawPolygonComplete}
        options={{
          drawingControl: true,

          circleOptions: {
          fillColor: "red",
          fillOpacity: 0.3,
          strokeWeight: 2,
          clickable: true,
          editable: true,
          zIndex: 1,
          draggable: true,
        },
          
          polygonOptions:{
          fillColor: "red",
          fillOpacity: 0.5,
          strokeWeight: 2,
          clickable: true,
          editable: true,
          zIndex: 1,
          draggable: true,
          },

          rectangleOptions:{
          fillColor: "red",
          fillOpacity: 0.3,
          strokeWeight: 2,
          clickable: true,
          editable: true,
          zIndex: 1,
          draggable: true,
          },

          drawingControlOptions:{
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [
            google.maps.drawing.OverlayType.CIRCLE,
            google.maps.drawing.OverlayType.POLYGON,
            google.maps.drawing.OverlayType.RECTANGLE,

          ],
          
        }}
        
        }
           
        />

        <Polygon onLoad={PonLoad} paths={paths} options={Poptions} />
        <Polygon
              onLoad={PonLoad}
              paths={isoLinePaths}
              options={isoptions}
        />
      </GoogleMap>
    </div>
  );
};

const defaultOptions = {
  strokeOpacity: 0.5,
  strokeWeight: 2,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
};

const middleOptions = {
  ...defaultOptions,
  zIndex: 2,
  fillOpacity: 0.3,
  strokeColor: "red",
  fillColor: "tomato",
};

export default Map;

// API key from client : AIzaSyAXSm-EQBZlX3_7WbKdcASeyQ6gLeyrR5g
// Working API KEY : AIzaSyCHRkRJPqEVolCNMRgkUMWj_KN_I-5p6Hk
