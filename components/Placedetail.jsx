import React from "react";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
} from "@reach/combobox";
import "@reach/combobox/styles.css";
import { useContext } from "react";
import { ProfileContext } from "../Service/ProfileProvider";

const Placedetail = ({ panTo }) => {
  const {userLocation, setZipCode} = useContext(ProfileContext);

  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    requestOptions: {
      location: { lat: () => userLocation.lat, lng: () => userLocation.lng },
      radius: 100 * 1000,
    },
  });
  return (
    <div
      className="modal fade"
      id="changeLocation"
      tabIndex="-1"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-body">
            {/*  */}
            <span
              className="closeiconpopup"
              data-bs-toggle="modal"
              data-bs-target="#changeLocation"
            >
              <img src="/assets/images/defaultimg/closepopup.png" alt="" />
            </span>
            <div className="popupouter changeLocationinfo">
              <div className="popupcontent">
                <h3>Change Location</h3>
                <p className="otpinfo">
                  For best results, use an address or ZIP located in your
                  desired advertising market area.
                </p>
                <p>Address or ZIP</p>
                <Combobox
                  onSelect={async (address) => {
                    setValue(address, false);
                    clearSuggestions();
                    try {
                      const results = await getGeocode({ address });
                      const { lat, lng } = await getLatLng(results[0]);
                      panTo({ lat, lng });
                    } catch (error) {
                      console.log("error!", error);
                    }
                  }}
                >
                  <ComboboxInput
                    className="form-control"
                    value={value}
                    onChange={(e) => {
                      setValue(e.target.value);
                      setZipCode(e.target.value);
                    }}
                    disabled={!ready}
                    placeholder="Enter a pincode or Adrees to better result"
                  />
                  <ComboboxPopover style={{ zIndex: "9999" }}>
                    <ComboboxList>
                      {status === "OK" &&
                        data.map(({ id, description }) => (
                          <ComboboxOption key={id} value={description} />
                        ))}
                    </ComboboxList>
                  </ComboboxPopover>
                </Combobox>
              </div>
              <div
                className="inp_row_opt "
              >
                <button
                  type="button"
                  className="defbutton"
                  data-bs-toggle="modal"
                  data-bs-target="#changeLocation"
                >
                  Submit
                </button>
              </div>
            </div>
            {/*  */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Placedetail;
