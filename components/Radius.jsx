import React from 'react'
import { useState, useContext } from "react"
import { ProfileContext } from '../Service/ProfileProvider'

const Radius = () => {
  const [minMile, setminMile] = useState(10)
  const [maxMile, setmaxMile] = useState(150)
  const {range, setRange} = useContext(ProfileContext);
  const setRadius = (value)=>{
    setRange(parseInt(value))
  }
  
  return (
    <div className='inp_wrap'>
        <div className="ringslider">
        <p><strong>Radius in Miles : {range} miles</strong></p>
            <div className="def_ringprice">
            <span>{minMile}</span>
            <span>{maxMile}</span>
            </div>
            <div className="ringslider_cont">
            <div className="slider-parent">
                <input type="range" value={range} min={minMile} max={maxMile} onChange= {(e)=>setRadius(e.target.value)}/>
            </div>

            </div>
        </div>
 
    </div>
  )
}

export default Radius 