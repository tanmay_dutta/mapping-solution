import React from "react";
import { useState, useContext } from "react";
import { ProfileContext } from "../Service/ProfileProvider";

function Radiusdrive() {
  const [minMile, setminMile] = useState(1);
  const [maxMile, setmaxMile] = useState(15);
  const { range, setRange } = useContext(ProfileContext);
  const setRadius = (value) => {
    setRange(parseInt(value));
  };
  return (
    <div className="ringslider">
      <p>
        <strong>Distance</strong>
      </p>
      <div className="def_ringprice">
        <span>{minMile}</span>
        <span>{maxMile}</span>
      </div>
      <div className="ringslider_cont">
        <div className="slider-parent">
        <input type="range" value={range} min={minMile} max={maxMile} onChange= {(e)=>setRadius(e.target.value)}/>
        </div>
      </div>
      <h4>{range}</h4>
      <p className="otpinfo">Estimated distance</p>
    </div>
  );
}

export default Radiusdrive;
