import React from 'react';
import styles from '../styles/Home.module.css';
import { useState, useContext, useEffect } from "react";
import axios from "axios";
import { ProfileContext } from '../Service/ProfileProvider';
import Loader from './Loader';

const Selectsummary = () => {
    const {userLocation, zipCode} = useContext(ProfileContext);
    const [homeCount, sethomeCount] = useState(0)
    const [poBoxCount, setpoBoxCount] = useState(0)
    const [appartmentCount, setappartmentCount] = useState(0)
    const [mailBoxCount, setmailBoxCount] = useState(0)
    const [businessCount, setbusinessCount] = useState(0)
    const [isloader, setIsLoader] = useState(true)
    
    useEffect(() => {
      
      if(zipCode.length === 0){
        console.log("zipcode is empty in select summary")

      }
      else{
        countsOfSummaries()
      }

    
    }, [zipCode, userLocation])
    

    
    const countsOfSummaries = async ()=>{
        await axios
            .get(`https://nodeserver.mydevfactory.com:3311/user/getSelectSummary?latitude=${userLocation.lat}&longitude=${userLocation.lng}&radius=15000&limit=500&offset=5`,{
              headers: { }
            })
            .then(async response => {
              if(Object.keys(response.data.data).length == 0){
              //  console.log("select summary 0")

              }
              else{
              // console.log("select summary response",response.data)
              sethomeCount(response.data.data.homeCount)
              setpoBoxCount(response.data.data.poBoxCount)
              setappartmentCount(response.data.data.appartmentCount)
              setmailBoxCount(response.data.data.mailBoxCount)
              setbusinessCount(response.data.data.businessCount)
              setIsLoader(false)


              }

              
            })
            .catch(function (error) {
    
           
              if (error.response) {
                console.log('response error seelect summary ===>', error.response);
                
              } else if (error.request) {
                console.log('Request Error==>', error.request);
              } else {
                console.log('Error occured', error.message);
              }
            });
    
            
    
      }
      if(Object.keys(userLocation).length == 0){
        return( 
          <div className={styles.map_content}>
            <div className={styles.map_heading}>
              <h2>Selection Summary</h2>
            </div>
            <div className={styles.mapdes}>
              <h3>Please Provide a zip code</h3>
            </div>
          </div>)
      }
      else{
        if(isloader)
        {
          return( 
          <div className={styles.map_content}>
            <div className={styles.map_heading}>
              <h2>Selection Summary</h2>
            </div>
            <div className={styles.mapdes}>
              <Loader />
            </div>
          </div>)
        }

       else{
  
            return (
              <div className={styles.map_content}>
                      <div className={styles.map_heading}>
                        <h2>Selection Summary</h2>
                        <p>Total Mailboxes {mailBoxCount}</p>
                      </div>
                      <div className={styles.mapdes}>
                        <p className={styles.mapinp_row}>
                          <strong>Home Addresses</strong>
                          <span>{homeCount}</span>
                        </p>
                        <p className={styles.mapinp_row}>
                          <strong>Apartment Addresses</strong>
                          <span>{appartmentCount}</span>
                        </p>
                        <p className={styles.mapinp_row}>
                          <strong>Business Addresses</strong>
                          <span>{businessCount}</span>
                        </p>
                        <p className={styles.mapinp_row}>
                          <strong>PO Box Addresses</strong>
                          <span>{poBoxCount}</span>
                        </p>
                      </div>
                    </div>
            )
          }
      }

 
      // console.log(parseInt(homeCount), poBoxCount, appartmentCount, mailBoxCount, businessCount)

      
      
}

export default Selectsummary