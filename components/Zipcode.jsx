import React, { useState } from "react";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
} from "@reach/combobox";
import "@reach/combobox/styles.css";
import { useContext } from "react";
import Budget from "./Budget";
import { ProfileContext } from "../Service/ProfileProvider";
import Radiusdrive from "./Radiusdrive";
import Demographics from './Demographics'

const Zipcode = ({ panTo }) => {


  const toggleCarousel = (action) => {
    const { Carousel } = require("bootstrap");
    const carousel = new Carousel("#myCarousel");
    if (action === "next") {
      carousel.next();
    } else {
      carousel.prev();
    }
  };


  const [userZipcode, setUserZipCode] = useState("")
  const { userLocation, setZipCode } = useContext(ProfileContext);
  // const []
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    requestOptions: {
      location: { lat: () => userLocation.lat, lng: () => userLocation.lng },
      radius: 100 * 1000,
    },
    debounce: 500,
  });
  
  if(userZipcode.length === 5){
    setZipCode(userZipcode)

  }
  return (
    <div className="modal fade" id="zipcodeid" tabIndex="-1" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-body">
            {/*  */}
            <span
              className="closeiconpopup"
              data-bs-toggle="modal"
              data-bs-target="#zipcodeid"
            >
              <img src="/assets/images/defaultimg/closepopup.png" alt="" />
            </span>

            <div className="popupouter zipcodeopt">
              <div
                id="myCarousel"
                className="carousel slide"
                data-bs-touch="false"
                data-bs-interval="false"
              >
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <div className="popupcontent zipcodeinfo">
                      <div className="popuplogo">
                        <img
                          src="/assets/images/defaultimg/mapicon.png"
                          alt=""
                        />
                      </div>
                      <h3>Enter an address or ZIP code</h3>
                      <p className="otpinfo">
                        For best results, use an address or ZIP located in your
                        desired advertising market area.
                      </p>
                      <div className="inp_wrap">
                        <div className="inp_row_opt ">
                          <p>Address or ZIP</p>
                          <div className="inp_rowopt2">

                            <Combobox
                              onSelect={async (address) => {
                                setValue(address, false);
                                clearSuggestions();
                                try {
                                  const results = await getGeocode({ address });
                                  const { lat, lng } = await getLatLng(results[0]);
                                  panTo({ lat, lng });
                                  
                                  
                                } catch (error) {
                                  console.log("error!", error);
                                }
                              }}
                              style={{ height: 50, width: 450}}
                            >
                              <ComboboxInput
                                className="form-control"
                                value={value}
                                onChange={(e) => {
                                  setValue(e.target.value);
                                  setUserZipCode(e.target.value)
                                  
                                }}
                                
                                disabled={!ready}
                                placeholder="Enter a pincode or Adrees to better result"
                              />
                              <ComboboxPopover style={{ zIndex: "9999" }}>
                                <ComboboxList>
                                  {status === "OK" &&
                                    data.map(({ id, description }) => (
                                      <ComboboxOption
                                        key={id}
                                        value={description}
                                      />
                                    ))}
                                </ComboboxList>
                              </ComboboxPopover>
                            </Combobox>

                            <div className="defbutton_opt">

                              <button type="button" className="defbutton"  onClick={() => {toggleCarousel("next");}} >Next</button>
                            </div>
                          </div>
                        </div>
                        <p className="morecont_opt">
                          <a
                            className="resendotp"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                          >
                            Cancel{" "}
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="popupcontent  budgetinfo">
                      <div className="popuplogo">
                        <img src="/assets/images/defaultimg/bug.png" alt="" />
                      </div>
                      <h3>What's your budget?</h3>
                      <p className="otpinfo">
                        Set an approximate <strong>budget</strong> to see how
                        many households you can reach you can always change this
                        later
                      </p>

                      <Budget />
                    </div>
                  </div>


                  <div className="carousel-item">
                           {/*  */}
                           <div className="popupcontent  areainfo">
                                <h3>What is your general business service area?</h3>
                                <p className="otpinfo">Choose a radius (miles) or drive-time (minutes) to identify your service area.</p>
                                <div  className="radio_option" >
                                  <div className="radio_info" >
                                      <input type="radio" id="test1" name="radio-group" checked />
                                      <label for="test1">Radius (miles)</label>
                                  </div>
                                  <div className="radio_info" >
                                      <input type="radio" id="test2" name="radio-group" />
                                      <label for="test2">Drive-time (minutes)</label>
                                  </div>
                                </div>
                                <div className="inp_wrap">
                                  <Radiusdrive />
                                  <div className="checkoption_view">
                                  
                                 </div>
                              
                                  <div className="defbutton_opt btnoptview">
                                      <button type="button" className="defbutton prevbtn"  onClick={() => toggleCarousel("prev")}>Previous</button>
                                    <button type="button" className="defbutton"  onClick={() => toggleCarousel("next")} >Next</button>
                                    </div>
                                </div> 

                            </div>
                           {/*  */}
                        </div>


                  <div className="carousel-item">
                    {/*  */}
                    <div className="popupcontent  adsinfo">
                      <h3>Who should see and receive your ads?</h3>
                      <p className="otpinfo">
                        Apply{" "}
                        <strong>
                          {" "}
                          <a href="#">up to 3 demographic filters</a>
                        </strong>{" "}
                        to narrow down your audience.
                      </p>

                      <Demographics />
                      <div className="defbutton_opt btnoptview">
                                      <button type="button" className="defbutton prevbtn"  onClick={() => toggleCarousel("prev")}>Previous</button>
                                    {/* <button type="button" className="defbutton"  data-bs-target="zipcodeid" >Next</button> */}
                      </div>
                      
                    </div>
                    {/*  */}
                  </div>
                </div>
              </div>
            </div>

            {/*  */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Zipcode;
