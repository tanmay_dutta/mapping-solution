import React from 'react';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.css';

import "bootstrap-icons/font/bootstrap-icons.css";

import Header from "./common/Header"
import Footer from "./common/Footer"
import Layout from "./common/Layout"
import { ProfileProvider } from '../Service/ProfileProvider';

function MyApp({ Component, pageProps }) {

  return <>
    <ProfileProvider>
      <Header />
        <Layout>
          <Component {...pageProps}  />
          </Layout>
      <Footer />
      </ProfileProvider>
  </>
}

export default MyApp
