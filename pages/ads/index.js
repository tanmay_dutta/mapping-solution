import styles from '../../styles/ads.module.css'
export default function ads() {
    return (
        <div className={styles.container}>
        <h1>Who should see and receive your ads?</h1>
        <h2>Apply <span><a href="">up to 3 demographic</a></span> filters to narrow down your audience.</h2>
        <p className={styles.line}></p>
        <div>
            <input type="radio"></input><p>Radius (miles)</p>
            <input type="radio"></input><p>Drive-time(minutes)</p>
        </div>
        <p>Household income range</p>
        <div>
            <p>$25k</p>
            <p>$50K</p>
            <p>$250k+</p>
        </div>
        <input type="range"></input>
        <p>Median Age Range</p>
        <div>
            <p>18 years</p>
            <p>35Y</p>
            <p>65+ Years</p>
        </div>
        <input type="range"></input>
        <div>
            <p>Home Ownership</p>
            <input type="checkbox"></input><p>All</p>
            <input type="checkbox"></input><p>Renter</p>
            <input type="checkbox"></input><p>Homeowner</p>
        </div>
        <div>
            <p>Presence of Children</p>
            <input type="checkbox"></input><p>All</p>
            <input type="checkbox"></input><p>None</p>
            <input type="checkbox"></input><p>Children</p>
        </div>

<div>
    <button className={styles.button}>Previous</button>
    <button className={styles.button}>Next</button>
</div>
        </div>
    )
}