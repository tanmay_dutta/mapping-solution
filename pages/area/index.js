import styles from '../../styles/area.module.css'
 export default function area() {
  return (
        <div className={styles.container}>
            <h1>What is your general business service area? </h1>
            <h2>Choose a radius (miles) or drive-time (minutes) to identify your service area.</h2>
            <p className={styles.line}></p>
            <div>
                <input type="radio" >
                    </input>
                <p>Radius</p>
                <input type="radio"></input>
                <p>Drive-time (minutes)</p>
            </div>
            <p>Distance</p>
            <input type="range"></input>
            <p>6</p>
            <p>Estimated distance</p>
            <p className={styles.line}></p>
            <div>
                <button className={styles.button}>Previus</button>
                <button className={styles.button}>Next</button>
            </div>
        </div>
    )
}