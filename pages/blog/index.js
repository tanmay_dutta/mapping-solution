import styles from '../../styles/blog.module.css';
import Link from "next/link";
import { useState } from 'react';
import{ DateObject , Calendar} from "react-multi-date-picker";
const Blogpage = () => {
  // const [value, onChange] = useState(new Date());
  const [date, setDate] = useState(
    new DateObject(),
  );

  function update(key, value) {
    date[key] += value;

    setDate(new DateObject(date));
  }

  const style = {
    display: "inline-block",
    width: "90px",
    fontSize: "16px",
  };

  return (
    <div  className="innercontent_outer">
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className={styles.bloginfo}>
              <div className={styles.inner_maincontent}>
                     {/*  blogadd*/}

                <div className={styles.blogadd}>
                  <div className={styles.blogadd_img}>
                    <img src="/assets/images/blogimg/bloglargeimg1.png" alt=""/>
                  </div>
                  <div className={styles.blogadd_info}>
                    <div className={styles.tranding_opt}>
                      <span>Tranding</span>
                    </div>
                    <h2><Link href="/blogdetails">As Taliban Crush Dissent, New Leaders Face Cascading Challenges</Link>  </h2>
                    <p>
                      Only one day after the Taliban named an acting cabinet to lead Afghanistan, the dizzying challenges that accompanied victory were coming into sharp focus. Tensions flared with Pakistan. Afghanistan's humanitarian crisis deepened. And the militants' brutal crackdown threatened to further erode public trust.
                    </p>
                    <div className={styles.post_add}>
                      <span>9 Sept , 2021</span>
                      <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                    </div>
                  </div>
                </div>
                     {/*  blogadd end*/}

                <div className={styles.allnews_opt}>
                  <div className={styles.heading_info}>
                    <h2>All News</h2>
                  </div>
                  <div className="all_post_item_wrap">
                    <div  className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogpost_img1.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails"> Praesent tempus tellus erat, vel dictum nunc cursus et. Vestibulum tristique at lorem sit amet iaculis.</Link>  </h3>
                        <p>
                          Only one day after the Taliban named an acting cabinet to lead Afghanistan, the dizzying challenges that accompanied victory were
                        </p>
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                    <div  className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogpost_img2.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails">Praesent tempus tellus erat, vel dictum nunc cursus et. Vestibulum tristique at lorem sit amet iaculis.</Link>  </h3>
                        <p>
                          Only one day after the Taliban named an acting cabinet to lead Afghanistan, the dizzying challenges that accompanied victory were
                        </p>
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>

                    <div  className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogpost_img3.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails">Praesent tempus tellus erat, vel dictum nunc cursus et. Vestibulum tristique at lorem sit amet iaculis.</Link> </h3>
                        <p>
                          Only one day after the Taliban named an acting cabinet to lead Afghanistan, the dizzying challenges that accompanied victory were
                        </p>
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>

                    <div  className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogpost_img4.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails">Praesent tempus tellus erat, vel dictum nunc cursus et. Vestibulum tristique at lorem sit amet iaculis. </Link></h3>
                        <p>
                          Only one day after the Taliban named an acting cabinet to lead Afghanistan, the dizzying challenges that accompanied victory were
                        </p>
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="allnews_opt overpostadd">
                  <div className={styles.heading_info}>
                    <h2>Overbefolkning </h2>
                    <div className={styles.viewMore_opt}>
                    <Link href="/blogdetails"><span>View More <i className="bi bi-arrow-right"></i></span></Link>
                    </div>
                  </div>
                  <div className="all_post_item_wrap">
                    <div className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogadd_img1.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails">Phasellus hendrerit, est ut faucibus pellentesque, diam velit ullam corper turpis, non curs</Link></h3>
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                    <div className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogadd_img2.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails">Suspendisse vehicula rutrum eleifend. Nunc sit amet magna odio. In vulputate magna quis mi pretium</Link></h3>
                       
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>

                    <div  className="all_post_item">
                      <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogimg/blogadd_img3.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                        <h3><Link href="/blogdetails">Phasellus hendrerit, est ut faucibus pellentesque, diam velit ulla mco rper turpis, non cursus sapien</Link></h3>
                      
                      </div>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>

                  
                  </div>
                </div>


                <div className="allnews_opt dusiness_add">
                  <div className={styles.heading_info}>
                    <h2>Business </h2>
                    <div className={styles.viewMore_opt}>
                    <Link href="/"><span>View More <i className="bi bi-arrow-right"></i></span></Link>
                    </div>
                  </div>
                  <div className={styles.dusiness_add_des_wrap}>
                    <div className={styles.dusiness_post}>
                      <div className={styles.dusiness_post_item}>
                        <div className={styles.post_item_pic}>
                          <img src="/assets/images/blogimg/grap_img1.png" alt=""/>
                        </div>
                        <div className={styles.post_item_pic_des}>
                          <h3><Link href="/blogdetails">Global Cryptocurrency Market Charts | CoinMarketCap</Link></h3>
                          <p>See the up-to-date total cryptocurrency</p>
                            <div className={styles.post_add}>
                            <span>9 Sept , 2021</span>
                            <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                          </div>
                        </div>
                      </div>
                      <div className={styles.dusiness_post_item}>
                        <div className={styles.post_item_pic}>
                          <img src="/assets/images/blogimg/grap_img2.png" alt=""/>
                        </div>
                        <div className={styles.post_item_pic_des}>
                          <h3><Link href="/blogdetails">Bitcoin, Ethereum, Crypto News and Price Data
                          </Link></h3>
                          <p>See the up-to-date total cryptocurrency</p>
                            <div className={styles.post_add}>
                            <span>9 Sept , 2021</span>
                            <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                          </div>
                        </div>
                     
                      </div>
                      <div className={styles.dusiness_post_item}>
                        <div className={styles.post_item_pic}>
                          <img src="/assets/images/blogimg/grap_img3.png" alt=""/>
                        </div>
                        <div className={styles.post_item_pic_des}>
                          <h3><Link href="/blogdetails">XRP - Bitcoin, Ethereum, Crypto News and Price Data</Link></h3>
                          <p>See the up-to-date total cryptocurrency</p>
                          <div className={styles.post_add}>
                            <span>9 Sept , 2021</span>
                            <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div className={styles.dusiness_add_grap}>
                      <div className={styles.add_grap_img}>
                        <img src="/assets/images/blogimg/grap_add.png" alt=""/>
                      </div>
                      <h3><Link href="/blogdetails">CoinMarketCap: Cryptocurrency Prices</Link></h3>
                      <p>Comprehensive and easy-to-use live cryptocurrency chart that tracks the movements of hundreds of cryptocurrencies.</p>
                          <div className={styles.post_add}>
                            <span>9 Sept , 2021</span>
                            <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                        </div>
                    </div>

                   
                  
                  </div>
                </div>





              </div>
              {/*  sidebar_info*/}
              <div className={styles.sidebar_info}>
                <div className={styles.blog_more_info}>
                  <div className={styles.blog_more_item}>
                    <div className={styles.blog_img_add}>
                      <img src="/assets/images/blogimg/blogimg1.png" alt=""/>
                    </div>
                    <div className={styles.blog_img_add_des}>
                      <h3> <Link href="/blogdetails">In vulputate magna quis mi pretium bibendum a sed leo. Fusce aliquam eleifend dolor.</Link> </h3>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                  </div>
                  <div className={styles.blog_more_item}>
                    <div className={styles.blog_img_add}>
                      <img src="/assets/images/blogimg/blogimg2.png" alt=""/>
                    </div>
                    <div className={styles.blog_img_add_des}>
                      <h3><Link href="/blogdetails">Phasellus hendrerit, est ut faucibus pellentesque, diam velit ullamcorper turpis, non cursus</Link> </h3>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                  </div>
                  <div className={styles.blog_more_item}>
                    <div className={styles.blog_img_add}>
                      <img src="/assets/images/blogimg/blogimg3.png" alt=""/>
                    </div>
                    <div className={styles.blog_img_add_des}>
                      <h3><Link href="/blogdetails">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquam</Link> </h3>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                  </div>
                  <div className={styles.blog_more_item}>
                    <div className={styles.blog_img_add}>
                      <img src="/assets/images/blogimg/blogimg4.png" alt=""/>
                    </div>
                    <div className={styles.blog_img_add_des}>
                      <h3><Link href="/blogdetails">Duis ullamcorper metus nisi, eu sodales magna elementum quis. Pellentesque tristique</Link></h3>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={styles.calendar_info}>
                          {/* <Calendar
                          onChange={onChange}
                          value={value}
                      /> */}

                      <Calendar 
                        value={new DateObject()} 
                      />
                </div>

                <div className={styles.newsletter_info}>
                  <h2>Newsletter Subscribe</h2>
                  <p>Email subscription is an opportunity to receive an interesting newsletter from a website</p>
                  <div className={styles.from_des}>
                      <div className={styles.inp_from_row}>
                          <input type="email" className={styles.form_control}  placeholder="Email address" />
                      </div>
                      <div className={styles.inp_from_row}>
                          <button type="submit" className="btn_opt Subscribe_btn">Subscribe Now</button>
                      </div>
                    
                  </div>

                </div>
                <div className={styles.followup_info}>
                    <div className={styles.heading_info}>
                        <h2>Follow us </h2>
                    </div>  
                    <div className={styles.social_cont}>
                      <Link href="/">
                      <div className="social_inp_row facebook_opt">
                        <span><i className="bi bi-facebook"></i> Facebook</span><strong>7.5K</strong>
                      </div>
                      </Link>
                      <Link href="/">
                      <div className="social_inp_row youtube_opt">
                        <span><i className="bi bi-youtube"></i> youtube</span><strong>7.5K</strong>
                      </div>
                      </Link>
                      <Link href="/">
                      <div className="social_inp_row twitter_opt">
                        <span><i className="bi bi-twitter"></i> Twitter</span><strong>7.5K</strong>
                      </div>
                      </Link>
                    </div>
                </div>
              </div>
                     {/*  sidebar_info end*/}
            </div>
          </div>
        </div>
      </div>
    </div>



  );
};

export default Blogpage;
