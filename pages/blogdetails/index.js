
import styles from '../../styles/blogdetails.module.css';
import Head from "next/head";
import Link from "next/link";

import React, { useState } from 'react';
const BlogDetailspage = (props) => {

  return (

<div className="innercontent_outer">
  <div className="container">
    <div className="row">
        <div className="col-sm-12">
            <div className="inner_main_content ">
                <div className={styles.innerbanner}>
                  <img src="/assets/images/blogdetails/bannerimg.png" alt=""/>
                </div>
                <div className="blog_details_info inner_cont_text border_bottom">
                    <div className={styles.postdate_opt}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                  <h2>As Taliban Crush Dissent, New Lea ders Face Cascading Challe.</h2>
                  <p>The most anticipated match in any IPL is the one between Mumbai Indians and Chennai Superkings. CSK has been playing like a dream whereas Mumbai Indians have been struggling with their batting. All fingers pointed to CSK coming out trumps but stranger things happen in cricket and what we viewed yesterday was one. The match swayed from one side to another before a master class from Kieron Pollard saw Mumbai Indians beating CSK off the last ball on the match.</p>

                  <p>It was the most thrilling encounter of IPL 2021 and Rohit Sharma was right in saying that it was one of the best matches he has been part of. </p>

                  <p>MS Dhoni summed it up correctly when he said that it was a game that CSK should have won if the bowlers didn’t bowl so many full tosses and if some of the catches were taken. But cricket is not a game of ifs and buts and the only thing that CSK can do is to learn from the mistakes and make the changes going forward.</p>
                  <h3>Duis a sagittis arcu. Integer vel pellentesque justo, fringilla scelerisque urna. </h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas quam sed tempus dapibus. Aenean finibus mi at diam aliquet, accumsan egestas sem hendrerit. Fusce gravida, sapien quis malesuada aliquet, nunc tortor iaculis arcu, non consequat lectus eros sit amet nunc. Pellentesque molestie velit vel egestas rhoncus. Ut venenatis orci ut urna egestas tempor. Integer risus metus, blandit sagittis odio et, tincidunt feugiat felis. Integer rhoncus gravida ipsum. Nam erat dolor, aliquet sed gravida id, ultrices sed enim. Mauris imperdiet convallis neque et elementum. Nam eget lectus erat. Nam et tortor eget ante egestas consectetur id rutrum justo. Praesent varius massa ut lectus maximus tincidunt. Fusce eget iaculis nisi, ut iaculis elit.</p>

                  <p>Nunc enim enim, tincidunt a diam non, lacinia tempor arcu. Suspendisse potenti. Pellentesque at elit lectus. Nulla quis semper lorem. Etiam vel arcu bibendum, rutrum sem placerat, venenatis felis. In nulla nibh, aliquam ac nulla vitae, dignissim porta lacus. Sed varius pharetra pulvinar. Suspendisse ultricies, ante vel sollicitudin vehicula, libero mauris ultricies dolor, venenatis scelerisque dui nisi sit amet turpis. Curabitur nec ex malesuada, gravida dui eget, tempor enim. Fusce ullamcorper id leo ac ullamcorper. Pellentesque sodales nec lectus et condimentum. Aliquam pharetra lorem sit amet aliquam placerat. Suspendisse consectetur vehicula magna. Proin maximus vel lectus a pharetra. Donec sed consectetur magna. Vestibulum in vulputate dui, sit amet scelerisque nunc.</p>
              <h3>Duis a sagittis arcu. Integer vel pellentesque justo, fringilla scelerisque urna. </h3>

                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas quam sed tempus dapibus. Aenean finibus mi at diam aliquet, accumsan egestas sem hendrerit. Fusce gravida, sapien quis malesuada aliquet, nunc tortor iaculis arcu, non consequat lectus eros sit amet nunc. Pellentesque molestie velit vel egestas rhoncus. Ut venenatis orci ut urna egestas tempor. Integer risus metus, blandit sagittis odio et, tincidunt feugiat felis. Integer rhoncus gravida ipsum. Nam erat dolor, aliquet sed gravida id, ultrices sed enim. Mauris imperdiet convallis neque et elementum. Nam eget lectus erat. Nam et tortor eget ante egestas consectetur id rutrum justo. Praesent varius massa ut lectus maximus tincidunt. Fusce eget iaculis nisi, ut iaculis elit.</p>

                  <p>Nunc enim enim, tincidunt a diam non, lacinia tempor arcu. Suspendisse potenti. Pellentesque at elit lectus. Nulla quis semper lorem. Etiam vel arcu bibendum, rutrum sem placerat, venenatis felis. In nulla nibh, aliquam ac nulla vitae, dignissim porta lacus. Sed varius pharetra pulvinar. Suspendisse ultricies, ante vel sollicitudin vehicula, libero mauris ultricies dolor, venenatis scelerisque dui nisi sit amet turpis. Curabitur nec ex malesuada, gravida dui eget, tempor enim. Fusce ullamcorper id leo ac ullamcorper. Pellentesque sodales nec lectus et condimentum. Aliquam pharetra lorem sit amet aliquam placerat. Suspendisse consectetur vehicula magna. Proin maximus vel lectus a pharetra. Donec sed consectetur magna. Vestibulum in vulputate dui, sit amet scelerisque nunc.</p>

<p>Duis a sagittis arcu. Integer vel pellentesque justo, fringilla scelerisque urna. Fusce nisl nulla, varius quis orci ut, consequat malesuada libero. Etiam in facilisis eros. Nam vel augue lectus. Morbi tempor nulla congue sem bibendum condimentum. Curabitur iaculis maximus ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec vitae fermentum risus. Nulla facilisi. Ut vel ullamcorper dui, blandit molestie nunc. Aliquam erat volutpat.</p>

<p>Sed vehicula quam vitae condimentum hendrerit. Vestibulum erat leo, maximus id tempus ac, luctus porttitor orci. Donec gravida sollicitudin dui lobortis finibus. Vivamus tempor, neque et pellentesque molestie, enim nunc tincidunt metus, vel tristique ex magna sed nisl. Etiam sed est enim. Sed sollicitudin libero nibh, sed lacinia augue pretium at. Nullam porttitor tincidunt felis ac posuere. Mauris efficitur tincidunt lectus, vitae auctor nisl porta quis. Nam eget elementum nunc. Etiam condimentum est ligula, a elementum odio porta et. Suspendisse blandit ipsum nulla, vel posuere turpis sodales ac. Aenean ac auctor erat. In sodales lobortis felis, vitae convallis orci. Nunc luctus lectus ac urna efficitur feugiat.</p>

<p>Maecenas diam sapien, ullamcorper vel lacus et, pretium consectetur eros. Mauris gravida molestie volutpat. Suspendisse nec tortor et leo accumsan sodales et sed massa. Praesent eget libero mollis purus iaculis mattis. Sed pretium magna vitae lacinia imperdiet. Aenean vel gravida nisi. Ut varius volutpat ligula in tincidunt. Sed laoreet lobortis suscipit. Morbi dapibus pharetra nisl eu euismod. In eu ipsum nec purus convallis bibendum. In congue vehicula turpis vitae vestibulum. Etiam porta est sem.lectus, vitae auctor nisl porta quis. Nam eget elementum nunc. Etiam condimentum est ligula, a elementum odio porta et. Suspendisse blandit ipsum nulla, vel posuere turpis sodales ac. Aenean ac auctor erat. In sodales lobortis felis, vitae convallis orci. Nunc luctus lectus ac urna efficitur feugiat.</p>
<p>lectus, vitae auctor nisl porta quis. Nam eget elementum nunc. Etiam condimentum est ligula, a elementum odio porta et. Suspendisse blandit ipsum nulla, vel posuere turpis sodales ac. Aenean ac auctor erat. In sodales lobortis felis,.</p>


                </div>
                <div className="related_blog_posts">
                  <div className="heading_info">
                    <h2>Related blog posts </h2>
                  </div>

                  <div className="all_post_item_wrap">
                    <div className="all_post_item">
                      <div className={styles.post_item_pic}>

                        <img src="/assets/images/blogdetails/recpost1.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>

                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                        <h3><Link href="/blogdetails">Phasellus hendrerit, est ut faucibus pellentesque.</Link></h3>
                      </div>
                     
                    </div>
                    <div className="all_post_item">
                    <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogdetails/recpost2.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>

                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                        <h3><Link href="/blogdetails">Loasellus hendrerit, est ut faucibus pellentesque.</Link></h3>
                      </div>
                    </div>
                    <div className="all_post_item">
                    <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogdetails/recpost3.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>
                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                        <h3><Link href="/blogdetails">Aucibus pellentesque diam admin</Link></h3>
                      </div>
                    </div>
                    <div className="all_post_item">
                    <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogdetails/recpost4.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>

                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                        <h3><Link href="/blogdetails">Aucibus pellentesque diam admin</Link></h3>
                      </div>
                    </div>

                    <div className="all_post_item">
                    <div className={styles.post_item_pic}>
                        <img src="/assets/images/blogdetails/recpost5.png" alt=""/>
                      </div>
                      <div className={styles.post_item_pic_des}>

                      <div className={styles.post_add}>
                        <span>9 Sept , 2021</span>
                        <strong><i className="bi bi-stopwatch-fill"></i> 5 min ago</strong>
                      </div>
                        <h3><Link href="/blogdetails">Aucibus pellentesque diam admin</Link></h3>
                      </div>
                    </div>

                  
                  </div>

                </div>
            </div>
        </div>
    </div>
  </div>
</div>


  );
};

export default BlogDetailspage;
