// import React, { useState } from 'react';   
import styles from '../../styles/footer.module.css';
import Link from "next/link";

const Footer = () => {
    
  const showModalLogin = () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#loginpopup");
    myModal.show();
  };

  const showModalRegister = () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#registerpopup");
    myModal.show();
  };

    return ( 
    <footer className={styles.footer_outer}>
        <div className="container">
            <div className="row">
                <div className="col_sm_12">
                    <div className={styles.footer_cont}>
                        <div className="footer_block footerlogo_des">
                            <div className={styles.footerlogo}>
                            {/* <Image src="/assets/images/logo.png" alt="" width={150} height={80}    /> */}

                            <Link href="/"> 
                <img src="/assets/images/defaultimg/logo.png" alt=""/>
                </Link>
                            </div>
                            <div className={styles.social_link}>
                                <ul>
                                    <li><Link href="/"><a><i className="bi bi-facebook"></i> Facebook</a></Link></li>
                                    <li><Link href="/"><a><i className="bi bi-twitter"></i> Twitter</a></Link></li>
                                    <li><Link href="/"><a><i className="bi bi-instagram"></i> Instagram</a></Link></li>
                                    <li><Link href="/"><a><i className="bi bi-google"></i> Google Review</a></Link></li>
                                </ul>
                            </div>
                        </div>
                        <div className={styles.footer_block}>
                            <div className={styles.headinginfo}>
                                Menu
                            </div>
                            <div className={styles.footerlink}>
                                <ul>
                                    <li><Link href="/mapview/">Map View</Link></li>
                                    <li><Link href="/blog/">Blog</Link></li>
                                    <li><Link href="/contactus/">Contact Us</Link></li>
                                    <li><Link href="/discovermaps/">Discover maps</Link></li>
                                </ul>
                            </div>
                        </div>
                        <div className={styles.footer_block}>
                            <div className={styles.headinginfo}>
                                Quick Links
                            </div>
                            <div className={styles.footerlink}>
                                <ul>
                                    {/* <li><Link href="/">Login</Link></li>
                                    <li><Link href="/">Register</Link></li> */}
                                    <li><a href="javascript:void(0)" onClick={showModalLogin}>Login</a></li>
                                    <li><a href="javascript:void(0)"  onClick={showModalRegister}>Register</a></li>
                                    <li><Link href="/testimonials/">Testimonials</Link></li>

                                </ul>
                            </div>
                        </div>
                        <div className={styles.footer_block}>
                            <div className={styles.headinginfo}>
                                Resources
                            </div>
                            <div className={styles.footerlink}>
                                <ul>
                                    <li><Link href="/termsconditions/">Terms & Conditions</Link></li>
                                    <li><Link href="/privacypolicy/">Privacy Policy</Link></li>
                                </ul>
                            </div>
                        </div>
                        <div className={styles.footer_block}>
                            <div className={styles.headinginfo}>
                                Contact Details
                            </div>
                            <div className={styles.footerlink + styles.addressinfo}>
                                <p><i className="bi bi-telephone-fill"></i> 52282980</p>
                                <p><i className="bi bi-envelope-fill"></i> info@hounsecounter.com</p>
                                    <p><i className="bi bi-geo-alt-fill"></i> 89 Wressle Road
                                    Place Newton YO17 8FL
                                    United Kingdom</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.copyright_info}>
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div className={styles.copyright_text}>
                        Copyright ©2022 <span>hounse counter</span>. All Rights Reserved.
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </footer>
    );
}
 
export default Footer;