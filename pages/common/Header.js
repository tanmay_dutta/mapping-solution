import styles from '../../styles/header.module.css';  
import Link from "next/link";
import React, { useState} from "react";
import {useRouter} from 'next/router'
import { useEffect } from 'react';
import Radius from '../../components/Radius';




const Header = () => {

  // menu content
  const [showMe, setShowMe] = useState(false);

  function toggle(){
    setShowMe(!showMe);
  }
  function handleClick(){
    setShowMe(!showMe);
  }



  // popup content
  const showModalmap = () => {
    // const { Modal } = require("bootstrap");
    // const myModal = new Modal("#shopmapview_opt");
    // myModal.show();
    alert("Comming Soon")
  };

  const showModalLogin = () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#loginpopup");
    myModal.show();
  };


  const showModalRegister = () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#registerpopup");
    myModal.show();
  };

  const showModalComoptpopup = () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#Comoptpopup");
    myModal.show();
  };



  const showModalchangepassword = () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#changepassword_popup");
    myModal.show();
  };

  const showModalresetpasword= () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#resetpasword");
    myModal.show();
  };

  const showModalzipcode= () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#zipcodeid");
   myModal.show();
 };

  const showchangeLocation= () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#changeLocation");
    myModal.show();
  };

  const showRediusdriveTime= () => {
    const { Modal } = require("bootstrap");
    const myModal = new Modal("#rediusdriveTime");
    myModal.show();
  };

  const [showProfile, setShowProfile] = useState(false);
  function toggleshowProfile(){
    setShowProfile(!showProfile);
  }


  const [hideLoginManu, sethideLoginManu] = useState(true);
  function toggleLoginManu(){
    sethideLoginManu(!hideLoginManu);
  }


  const [showProfileManu, setShowProfileManu] = useState(false);
  function toggleshowProfileManu(){
    setShowProfileManu(!showProfileManu)
  }

  const [hidedemoGraphics, setHideDemographics] = useState(false);
  function toggleDemographics(){
    setHideDemographics(!hidedemoGraphics)
  }

// rangoption

/* Registration METHOD: Post url: /user/Registration */

  const [firstName,setfirstName] = useState("")
  const [lastName,setlastName] = useState("")
  const [companyName, setCompanyName] = useState("")
  const [phoneNumber, setPoneNumber] = useState("")
  const [address, setAddress] = useState("")
  const [address2, setAddress2] = useState("")
  const [city, setCity] = useState("")
  const [zipCode, setZipCode] = useState("")
  const [userState, setUserState] = useState("")
  const [email,setEmail] = useState("")
  const [password,setPassword] = useState("")
  const [confirmPassword,setconfirmPassword] = useState("")

  const userSignup = async(e)=>{
    e.preventDefault()

    const res = await fetch(`https://nodeserver.mydevfactory.com:3311/user/registration`,{
      method: "POST",
      headers:{
        "Content-Type":"application/json"
      },

      body:JSON.stringify({
        "first_name": firstName,
        "last_name": lastName,
        "company_name": companyName,
        "mobile_no": phoneNumber,
        "address": address,
        "address2": address2,
        "city": city,
        "state": userState,
        "zip_code": zipCode,
        "email": email,
        "password": password,
        "confirm_password": confirmPassword 
      })
    })
    const res2 = await res.json()
    if(res2.error){
      console.log("error occured",res2.message)
    }
    else{
      if(res2.success === false){
        if(res2.message === "Email already registered.")
        { 
          alert(res2.message)
        }
      }
        else{
        alert(res2.message+" Please enter your details to log in")
      }
    }
  }

  // Login Method

    const [useremail,setuserEmail] = useState("")
    const [userPassword,setuserPassword] = useState("")
    const[userID, setUserID] = useState(0)
    const [emailOtp, setEmaiOtp] = useState("")
    const[name, setName] = useState("")
    const[loginMessage, setloginMessage] = useState("")
    
    let profileName=""


    useEffect(() => {
      const firstname = localStorage.getItem('firstName')
      const lastname = localStorage.getItem('lastName')
      const udata = localStorage.getItem('data')

      if(!udata){
        showModalLogin()
        toggleDemographics()
      }
      else{
          
          const fname = JSON.parse(firstname)
          const lname = JSON.parse(lastname)
          const ldata = JSON.parse(udata)

          toggleLoginManu()
          profileName = fname+' '+lname
          setuserEmail(ldata.email)
          setName(profileName)
          toggleshowProfileManu()
                    

      }


    },[]);

    let tempid = 0
    const router  = useRouter()
    const userLogin = async (e)=>{
      e.preventDefault()

      const res =  await fetch(`https://nodeserver.mydevfactory.com:3311/user/login`,{
        method:"POST",
        headers:{
          "Content-Type":"application/json"
        },
        body:JSON.stringify({
          "email": useremail,
          "password": userPassword
        })
      })

        const res2 = await res.json()
      if(res2.error)
      {
        alert(res2.message)
      }
      else{
        if(res2.success === false){
          setloginMessage(res2.message)
          showModalLogin()

        }else{
          setHideDemographics(false)
          toggleshowProfileManu()
          toggleLoginManu()        
          profileName = res2.data.firstName+' '+res2.data.lastName
          setName(profileName)
          localStorage.setItem('firstName', JSON.stringify(res2.data.firstName))
          localStorage.setItem('lastName', JSON.stringify(res2.data.lastName))
          localStorage.setItem('data', JSON.stringify(res2.data))
          setTimeout(() => {
            showModalzipcode()
        }, 1000);
          
        }
        
      }

    }

    // forgotPasswor

    const forgotPassword = async (e)=>{
      e.preventDefault()

      const res =  await fetch(`https://nodeserver.mydevfactory.com:3311/user/forgotPasswordSendOtp`,{
        method:"POST",
        headers:{
          "Content-Type":"application/json"
        },
        body:JSON.stringify({
          "email": useremail
        })
      })

      const res2 = await res.json()

      if(res2.type === "Validation error"){
          alert(res2.message+ " Please complete your registration")
          showModalRegister()
        }
        
      
      else{
        alert(res2.message)
        tempid = res2.data.id
        setUserID(tempid)
        showModalComoptpopup()
       
      }
    }

    const otpVeriFication = async (e)=> {

      e.preventDefault()
      
      const res = await fetch(`https://nodeserver.mydevfactory.com:3311/user/verifyEmailOtp`,{
        method:"POST",
        headers:{
          "Content-Type":"application/json"
        },
        body:JSON.stringify({
          "user_id" : userID,
          "email_otp" : emailOtp
        })
      })

      const res2 = await res.json()
      if(res2.error){
        console.log("error occured "+error)
      }
      else{
        if (res2.success === false) {
          alert(res2.message)
          showModalresetpasword()
        } else {
          alert(res2.message)
          showModalchangepassword()
        }
      }
    }
  const [userNewPassword, setUserNewPassword] = useState()
  const [retypePassword, setRetypePassword] = useState()
    const chngPassword = async(e)=> {
      e.preventDefault()

      const res = await fetch(`https://nodeserver.mydevfactory.com:3311/user/changePassword`,{
        method: "POST",
        headers:{
          "Content-Type":"application/json"
        },

        body:JSON.stringify({
          "user_id" : userID,
          "new_password": userNewPassword,
          "retype_password": retypePassword

        })
      })
      const res2 = await res.json()
      if(res2.error){
        console.log(res2.message)
      }
      else{
        if(res2.success === false){
          alert(res2.message)
          

        }
        else{
          alert(res2.message)
          router.push('/')
        }
      }
      }
    
      const logout = ()=>{
        localStorage.clear()
        setName("")
        setShowProfileManu(true)
        toggleshowProfileManu()
        toggleDemographics()
        location.reload()
        toggleLoginManu()

      }


  return (
      <div className={styles.headerouter}> 
      <div className="container"> 
        <div className="row">
            <div className="col-sm-12">
              <div className={styles.header_cont}>
                <div className={styles.manuicon} onClick={toggle}><i className="bi bi-list"></i></div>
                <div className={styles.logo}>
                <Link href="/"> 
                <img src="/assets/images/defaultimg/logo.png" alt=""/>
                </Link>

                </div>
                <div className={styles.header_right}>
                    <div className={styles. header_nav}>
                      <ul>
                        {/* <li><a onClick={showModalmap}>Map View</a></li> */}
                        <li><Link href="/blog/">Blog</Link></li>
                        <li><Link href="/contactus/">Contact Us</Link></li>
                        <li><Link href="/discovermaps/">Discover maps</Link></li>
                      </ul>
                    </div>
                    <div className={styles.login_option} style={{display:hideLoginManu?"block":"none"}}>
                      <div className={styles.login_view}>
                      <a className="defbtn activebtn" onClick={showModalLogin}><i className="bi bi-person-fill"></i>Login</a>
                      <a className="defbtn" onClick={showModalRegister}><i className="bi bi-person-plus-fill"></i>Register</a>


                      </div>
                    </div>
                     <div className={styles.afterlogin_cont} style={{display:showProfileManu?"block":"none"}}>
                     <div className={styles.afterlogin_info}>
                            <div className={styles.profileicon}>
                              <img src="/assets/images/defaultimg/profilepic.png" alt=""/>
                            </div>
                            <div className={styles.profile_content}>
                                <h3>{name}</h3>
                                <p>{useremail}</p>
                                <div className={styles.dropdownprofile} onClick={toggleshowProfile}>
                                    <i className="bi bi-chevron-down"></i>
                                </div>
                            </div>
                            <div className={styles.profile_dropdowninfo} style={{display:showProfile?"block":"none"}}>
                                <ul>
                                    <li><Link href="/myaccount">My account</Link></li>
                                    <li onClick={(e)=>logout(e)}><Link href="/">Logout</Link></li>
                                </ul>
                            </div>
                        </div>


                    </div>
                </div>
              </div>
            </div>
         </div>
      </div>
      <div className={styles.submenu}>
      <div className="container"> 
        <div className="row">
            <div className="col-sm-12">
              <div className={styles.submenu_content}>
                <ul>
                  <li><a onClick={showchangeLocation}><i className="bi bi-geo-alt-fill"></i> Change Location</a></li>
                  <li style={{display:hidedemoGraphics?"none":"block"}}><a onClick={showModalzipcode}><i className="bi bi-people-fill"></i> Demographics</a></li>
                  <li><a onClick={showRediusdriveTime}><i className="bi bi-pin-map-fill"></i> Radius Drive Time</a></li>
                  {/* <li><Link href="/"><a><i className="bi bi-person-workspace"></i> Show recomendations</a></Link></li> */}
                  <li><Link href="/"><a><i className="bi bi-boxes"></i> Businesss (0)</a></Link></li>
                  <li><Link href="/"><a><i className="bi bi-boxes"></i> Po Boxes (0)</a></Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={styles.mobile_menudetails} id="mobilemenu"   style={{display:showMe?"block":"none"}}>
              <div className={styles.menuclose} onClick={handleClick}>
              <i className="bi bi-x-circle"></i>

              </div>
                    <ul>
                        <li><Link href="/mapview"><a><i className="bi bi-map-fill"></i> Map View</a></Link></li>
                        <li><Link href="/blog"><a><i className="bi bi-signpost-fill"></i> Blog</a></Link></li>
                        <li><Link href="/contactus"><a><i className="bi bi-person-lines-fill"></i> Contact Us</a></Link></li>
                        <li><Link href="/biscover_maps"><a><i className="bi bi-diagram-2-fill"></i> Discover maps</a></Link></li>
                    </ul>
                  <ul>
                  <li><Link href="../../components/Placedetail"><a><i className="bi bi-geo-alt-fill"></i> Change Location</a></Link></li>
                  <li><Link href="/demographics"><a><i className="bi bi-people-fill"></i> Demographics</a></Link></li>
                  <li><Link href="/rediusDrive"><a><i className="bi bi-pin-map-fill"></i> Redius Drive Time</a></Link></li>
                  <li><Link href="/showrecomendations"><a><i className="bi bi-person-workspace"></i> Show recomendations</a></Link></li>
                  <li><Link href="/businesss"><a><i className="bi bi-boxes"></i> Businesss (0)</a></Link></li>
                  <li><Link href="/poboxes"><a><i className="bi bi-boxes"></i> Po Boxes (0)</a></Link></li>
                </ul>

      </div>
      


      {/*  shopmapview_opt */}

{/* <div className="modal fade" id="shopmapview_opt" tabIndex="-1" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                  <span
                  className="closeiconpopup"
                  data-bs-toggle="modal"
                  data-bs-target="#loginpopup"
                  >
                  <img src="/assets/images/defaultimg/closepopup.png" alt="" />
                  </span>

                <div className="popupouter shopmapview" >
                    <div className="popupcontent">
                        <div className="search_map_view" >
                          <div className="search_map_opt">
                              <div className='inp_text_view'>
                                <input type="text" className="form-control" placeholder='Serach'  />
                                <button type="button" className="search_btn"><i className="bi bi-search"></i></button>
                              </div>

                          </div>
                          <div className='inp_row_opt'>
                                <button type="submit" className="defbutton">Ok</button>
                            </div>
                        </div>

                      

                    </div>
                  </div>

            
            </div>
          </div>
        </div>
      </div> */}


{/*  loginpopup */}

<div className="modal fade" id="loginpopup" tabIndex="-1" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                {/*  */}
                  <span
                  className="closeiconpopup"
                  data-bs-toggle="modal"
                  data-bs-target="#loginpopup"
                  >
                  <img src="/assets/images/defaultimg/closepopup.png" alt="" />
                  </span>
                <div className="popupouter loginpopup" >
                    <div className="popupcontent">
                        <div className='popuplogo'>
                            <img src="/assets/images/defaultimg/logo.png" alt=""/>
                        </div>
                        <h3>Sign in to your account</h3>
                        <h3>{loginMessage}</h3>

                        <form onSubmit={(e)=>userLogin(e)}>
                          <div className='inp_wrap'>
                                  <div className='inp_row_opt'>
                                      <label>Username</label>
                                      <div className='inp_text_view'>
                                          <span><img src="/assets/images/defaultimg/user.png" alt=""/></span>
                                          <input type="text" valeu={useremail} onChange={(e)=>setuserEmail(e.target.value)} className="form-control" placeholder='jhon.deo@gmail.com'  />
                                      </div>
                                  </div>
                                  <div className='inp_row_opt'>
                                      <label>Password</label>
                                      <div className='inp_text_view'>
                                      <span><img src="/assets/images/defaultimg/logout.png" alt=""/></span>
                                          <input type="password" valeu={userPassword} onChange={(e)=>setuserPassword(e.target.value)}  className="form-control"  placeholder='******' />
                                      </div>
                                  </div>
                                  <div className='chekout_opt'>
                                      
                                      <p onClick={showModalresetpasword} data-bs-dismiss="modal"
                                aria-label="Close">Forgot Password?</p>
                                  </div>
                                  <div className='inp_row_opt'>
                                  <button type="submit" className="defbutton" data-bs-dismiss="modal">Sign In</button>
                                  </div>
                          </div>
                        </form>

                    </div>
                  </div>
                {/*  */}
            
            </div>
          </div>
        </div>
      </div>
{/*  */}

{/* Register   */}

  <div className="modal fade" id="registerpopup" tabIndex="-1" aria-hidden="true" >
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-body">

        <span className="closeiconpopup" data-bs-toggle="modal" data-bs-target="#registerpopup"><img src="/assets/images/defaultimg/closepopup.png" alt=""/></span>
            {/*  */}
            <div className="popupouter resitrationinfo" >
                <div className="popupcontent">
                  <div className='popuplogo'>
                      <img src="/assets/images/defaultimg/logo.png" alt=""/>
                  </div>
                  <h3>Create your account</h3>
                  <form onSubmit={(e)=>userSignup(e)}>
                  <div className='inp_wrap'>
                          <div className='inp_row_opt'>
                              
                              <div className='inp_text_view input-gap'>
                                  <span><i className="bi bi-person" style={{fontSize: 20}}></i></span>
                                  <input type="text" value={firstName} pattern="[a-zA-Z]{1,20}" title="Please Enter a valid name" onChange={(e)=>setfirstName(e.target.value)} className="form-control" placeholder='First Name' required/>
                                  
                                  
                                    <span><i className="bi bi-person" style={{fontSize: 20}}></i></span>
                                    <input type="text" value={lastName} pattern="[a-zA-Z]{1,20}" title="Please Enter a valid name" onChange={(e)=>setlastName(e.target.value)} className="form-control"  placeholder='Last Name' required/>
                                 

                                  <div className="invalid-feedback">
                                    Name is required
                                  </div>
                                  
                              </div>
                          </div>

                          <div className='inp_row_opt'>
                              
                              <div className='inp_text_view'>
                                  <span><i className="bi bi-building" style={{fontSize: 20}} ></i></span>
                                  <input type="text" value={companyName} pattern="[a-zA-Z\s]{1,20}" title="Please Enter a valid Company Name" onChange={(e)=>setCompanyName(e.target.value)} className="form-control"  placeholder='Company Name' required/>

                                  <span><i className="bi bi-phone" style={{fontSize: 20}}></i></span>
                                  <input type="text" value={phoneNumber} pattern="[\d]{10}" title="Please Enter a valid Phone Number" onChange={(e)=>setPoneNumber(e.target.value)} className="form-control"  placeholder='Phone' required/>
                              </div>
                              
                          </div>

                          <div className='inp_row_opt'>
                              <div className='inp_text_view'>
                              <span><i className="bi bi-envelope" style={{fontSize: 20}}></i></span>
                                  <input type="text" value={email}  pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$" title="Please Enter a valid email id" onChange={(e)=>setEmail(e.target.value)} className="form-control"  placeholder= 'jhon.doe@mail.com' required/>
                                  
                              </div>
                          </div>

                          <div className='inp_row_opt'>

                              <div className='inp_text_view'>
                              <span><i className="bi bi-geo-alt"  style={{fontSize: 20}}></i></span>
                                  <input type="text" value={address} pattern="[a-zA-Z\d\s]{1,30}" title="Please Enter a valid Address" onChange={(e)=>setAddress(e.target.value)} className="form-control"  placeholder='Address' />


                            
                              <span><i className="bi bi-geo-alt"  style={{fontSize: 20}}></i></span>
                                  <input type="text" value={address2} pattern="[a-zA-Z\d\s]{1,30}" title="Please Enter a valid Address" onChange={(e)=>setAddress2(e.target.value)} className="form-control"  placeholder='Address 2' />
                              

                              </div>
                          </div>

                          <div className='inp_row_opt'>
                              <div className='inp_text_view'>
                              <span><i className="bi bi-geo-alt"  style={{fontSize: 20}}></i></span>
                                  <input type="text" value={city} pattern="[a-zA-Z\s]{1,20}" title="Please Enter a valid City" onChange={(e)=>setCity(e.target.value)} className="form-control"  placeholder='city' />

                                  <span><i className="bi bi-geo-alt"  style={{fontSize: 20}}></i></span>
                                  <input type="text" value={zipCode} pattern="[a-zA-Z\s]{1,20}" title="Please Enter a valid name" onChange={(e)=>setZipCode(e.target.value)} className="form-control"  placeholder='State' />

                                   <span><i className="bi bi-geo" style={{fontSize: 20}}></i></span>
                                  <input type="text" value={userState} title="Please Enter a valid ZIP Code" onChange={(e)=>setUserState(e.target.value)} className="form-control"  placeholder='Zip code' required/>     

                              </div>
                          </div>
                        <div className='inp_row_opt'>
                              <div className='inp_text_view'>
                              <span><i className="bi bi-unlock" style={{fontSize: 20}}></i></span>
                                  <input type="password" value={password} onChange={(e)=>setPassword(e.target.value)} pattern="^[A-Za-z]\w{7,14}$"
                                  title="password should be 7 to 15 characters starts with a letter, can contain numeric digits, underscore" className="form-control"  placeholder='Password' required/>
                              </div>
                          </div>

                          <div className='inp_row_opt'>
                              <div className='inp_text_view'>
                              <span><i className="bi bi-unlock" style={{fontSize: 20}}></i></span>
                                  <input type="password" value={confirmPassword} onChange={(e)=>setconfirmPassword(e.target.value)} className="form-control"  placeholder='confirm password'/>
                              </div>
                          </div>
                          <div className='chekout_opt'>
                              <div className="form-check">
                                  <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                  <label className="form-check-label" htmlFor="exampleCheck1">I agree to all the</label>
                                   <label data-bs-dismiss="modal"
                              aria-label="Close"> <Link href="/termsconditions">Terms , Privacy Policy.  </Link></label>
                              </div>
                          </div>
                      
                          <div className='inp_row_opt'>
                          <button type="submit" className="defbutton" >Create an account</button>
                          </div>
                          <p className="morecont_opt">Already a member? <a onClick={showModalLogin} data-bs-dismiss="modal"
                              aria-label="Close"> Log In</a></p>
                  </div>
                  </form>
                </div>
              </div>
            {/* */}
        
        </div>
      </div>
    </div>
  </div>
{/*  */}

{/* Comoptpopup   */}

  <div className="modal fade" id="Comoptpopup" tabIndex="-1" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                {/*  */}
                <div className="popupouter confirmPassword">
                        <div className="popupcontent">

                        <div className='popuplogo'>
                            <img src="/assets/images/defaultimg/logo.png" alt=""/>
                        </div>
                        <h3>Confirm Password</h3>
                        <p className="otpinfo">OTP send your register email address please check your email and submit OTP for continue</p>
                        <form onSubmit={(e)=>otpVeriFication(e)}>
                          <div className='inp_wrap'>
                                  <div className='inp_row_opt otpinput'>
                                      <div className='inp_text_view'>
                                          <input type="text" value={emailOtp} onChange={(e)=>setEmaiOtp(e.target.value)} className="form-control"  placeholder='12345' />
                                      </div>
                                      <div className='inp_text_view'>
                                          <input type="hidden" value={userID}/>
                                      </div>
                                      <button type="submit" className="defbutton" data-bs-dismiss="modal">Submit</button>

                                  </div>
                                <p className="morecont_opt"><a className="resendotp" > Resend</a></p> 
                          </div>
                        </form>
                        </div>
                </div>
                {/*  */}
            
            </div>
          </div>
        </div>
  </div>
{/*  */}

{/* Change password   */}

<div className="modal fade" id="changepassword_popup" tabIndex="-1" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                {/*  */}
                <div className="popupouter changepassword"  >
                    <div className="popupcontent">
                      
                        <div className='popuplogo'>
                            <img src="/assets/images/defaultimg/logo.png" alt=""/>
                        </div>
                        <h3>Change password</h3>
                        <form onSubmit={(e)=>chngPassword(e)}>
                        <div className='inp_wrap'>
                          <div className='inp_row_opt'>
                              <label>New Password<span>*</span></label>
                              <div className='inp_text_view'>
                                  <input type="password" value={userNewPassword} onChange={(e)=>setUserNewPassword(e.target.value)} pattern="^[A-Za-z]\w{7,14}$"
                                  title="password should be 7 to 15 characters starts with a letter, can contain numeric digits, underscore" 
                                  className="form-control"  placeholder='******' />
                              </div>
                          </div>
                          <div className='inp_row_opt'>
                              <label>Retype Password<span>*</span></label>
                              <div className='inp_text_view'>
                                  <input type="password" value={retypePassword} onChange={(e)=>setRetypePassword(e.target.value)} className="form-control"  placeholder='******' />
                              </div>
                          </div>
                          <div className='chekout_opt'>
                              <div className="form-check1">
                                  At least 8 Character 
                              </div>
                              <div className="progresspassword">
                                  <div className="progress">
                                      <div className="progress-bar" role="progressbar" aria-valuenow="70"
                                      aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                      <span className="sr-only">  </span>
                                      </div>
                                  </div>
                                  Stong
                              </div>
                                                          
                          </div>
                          <div className='inp_row_opt'>
                          {/* <button type="submit" className="defbutton"  onClick={showModalresetpasword}  data-bs-dismiss="modal"
                              aria-label="Close" >Change Password</button> */}
                              <button type="submit" className="defbutton" data-bs-dismiss="modal">Change Password</button>
                          </div>
                        </div>
                        </form>
                    </div>
                  </div>
                {/*  */}
            
            </div>
          </div>
        </div>
  </div>
{/*  */}


{/* resetpasword   */}

<div className="modal fade" id="resetpasword" tabIndex="-1" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                {/*  */}
                <div className="popupouter reset_pasword">
                    <div className="popupcontent">

                        <div className='popuplogo'>
                          <img src="/assets/images/defaultimg/logo.png" alt=""/>
                        </div>
                        <h3>Reset Your Password</h3>
                        <p className="otpinfo">Fear not. We'll email you instructions to reset your password. If you 
                        don't have access to you: email anymore. you can try <a href="">account recovery</a>.</p>
                        <form onSubmit={(e)=>forgotPassword(e)}>
                          <div className='inp_wrap'>
                            <div className='inp_row_opt '>
                                <p>Username/Email</p>
                                <div className="inp_rowopt2">
                                  <div className='inp_text_view'>
                                      <input type="text" value={useremail} onChange={(e)=>setuserEmail(e.target.value)}  className="form-control"  placeholder='jhon.deo@gmail.com' />
                                  </div>
                                  <div className="defbutton_opt">
                                  <button type="submit" className="defbutton" data-bs-dismiss="modal"
                              aria-label="Close">Reset Password</button>
                                  </div>
                                </div>
                                
                                

                            </div>
                            <p className="morecont_opt">Return to <a className="resendotp" onClick={showModalLogin}  data-bs-dismiss="modal"
                                aria-label="Close"  >Login </a></p> 
                          </div>
                        </form>
                    </div>
                  </div>

                {/*  */}
            
            </div>
          </div>
        </div>
  </div>
{/*  */}

{/* rediusdriveTime   */}

<div className="modal fade" id="rediusdriveTime" tabIndex="-1" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                {/*  */}
                <span className="closeiconpopup" data-bs-toggle="modal" data-bs-target="#rediusdriveTime"><img src="/assets/images/defaultimg/closepopup.png" alt=""/></span>   
                <div className="popupouter reset_pasword">
                    <div className="popupcontent">
                        <h3>Radius Drive Time</h3>
                        <p className="otpinfo">For best results, use an address or ZIP located in your desired advertising market area.</p>
                        <Radius />
                        <div className='inp_row_opt'>
                                {/* <button type="submit" className="defbutton">Submit</button> */}
                                <button type="submit" className="defbutton" 
                                data-bs-toggle="modal" data-bs-target="#rediusdriveTime">Submit</button>
                        </div>
                    </div>
                  </div>

                {/*  */}
            
            </div>
          </div>
        </div>
  </div>
{/*  */}

    </div>

  );
};
export default Header;

