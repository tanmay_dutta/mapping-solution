
// import React from 'react'
import styles from '../../styles/contactus.module.css';
import Head from "next/head";
import Link from "next/link";


const ContactUsPage = (props) => {
  return (
      <div className="innercontent_outer">
        <div className="container">
          <div className="row">
              <div className="col-sm-12">
                  <div className="inner_main_content ">
                      <div className={styles.contactus_banner}>
                          <h2>Contact us</h2>
                          <p>Our collection sets standards in terms of exclusivity and style of exquisite carpets</p>
                      </div>

                      <div className={styles.contact_des}>
                        <div className={styles.contact_des_left}>
                          <div className={styles.contact_des_add}>
                              <img src="/assets/images/contactus/contactus_img.png" alt=""/>
                          </div>
                          <div className={styles.contact_map}>
                              <img src="/assets/images/contactus/mapicon.png" alt=""/>
                          </div>
                        </div>
                        <div className={styles.contact_des_right}>

                          <div className={styles.contact_info}>

                              <h2>Contact Us </h2>
                              <div className={styles.contact_info_block}>

                              {/* <i className="bi bi-geo-alt-fill"></i> */}
                                <i className="bi bi-geo-alt"></i>

                                <h3>Address</h3>
                                <h4>uperadviceaustralia</h4>
                                <p>Dienerreihe 420457 Hamburg <span>Germany</span></p>
                              </div>
                              <div className={styles.contact_info_block}>

                                <i className="bi bi-telephone"></i>
                                <h3>Number</h3>
                                <p>+49 40 33 73 80 (89)</p>
                              </div>

                          </div>
                        </div>


                      </div>

                      <div className={styles.contact_fromdes}>

                        <h2>Get in Touch</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content</p>
                        <div className={styles.contactus_info}>

                            <div className="inp_row opt2">
                              
                                <div className="form-floating">
                                  <input type="text" className="form-control" id="fristname" placeholder="First Name" name="text" />
                                  <label htmlFor="fristname">First Name</label>
                                </div>
                                <div className="form-floating">
                                  <input type="text" className="form-control" id="lastname" placeholder="Last Name" name="lastname" />
                                  <label htmlFor="lastname">Last Name</label>
                                </div>
                            </div>
                            <div className="inp_row opt2">
                                <div className="form-floating">
                                  <input type="email" className="form-control" id="email" placeholder="Email" name="email" />
                                  <label htmlFor="email">maddisonsharp124@gmail.com</label>
                                </div>
                                <div className="form-floating">
                                  <input type="tel" className="form-control" id="phone" placeholder="Phone Number" name="phone" />
                                  <label htmlFor="phone">Phone Number</label>
                                </div>
                            </div>
                            <div className="inp_row">
                                <div className="form-floating">
                                  <textarea className="form-control" id="comment" name="text" placeholder="Write Message Here.."></textarea>
                                  <label htmlFor="comment">Write Message Here..</label>
                                </div>
                            </div>
                            <div className={styles.inp_from_row}>
                              <button type="submit" className="btn_opt Subscribe_btn">Subscribe Now</button>
                          </div>

                          

                        </div>

                      </div>

                      
                  </div>
              </div>
          </div>
        </div>
      </div>
    );
};

export default ContactUsPage;
