import React from 'react'
import Map from '../../components/Map';
import styles from '../../styles/area.module.css'

const MapView = (props) => {
  return (
    <div className={styles.mapView}>
       <Map />
    </div>
    
  );
};    

export default MapView;
