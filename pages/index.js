import styles from "../styles/Home.module.css";
import Map from "../components/Map";
import { useState, useContext } from "react";
import { useEffect } from "react";
import { ProfileContext } from "../Service/ProfileProvider";
import Selectsummary from "../components/Selectsummary";
export default function Home() {
  const [coordinates, setCoordinates] = useState({
    lat: 37.78825,
    lng: -122.4324,
  });

  useEffect(() => {

    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        setCoordinates({ lat: latitude, lng: longitude });
      }
    );


  }, []);

 

  return (
    <div className={styles.mapouter}>
      <div className={styles.map_conttent_area}>
        <div className={styles.map_description}>
          <Selectsummary />
        </div>
        <Map coordinates={coordinates} setCoordinates={setCoordinates} />
      </div>
    </div>
  );
}
