import styles from '../../styles/myaccount.module.css';
import React from "react";
import {useState, useEffect} from "react";


const MyAccountPage = () => {

  const[userEmail, setUserEmail] = useState("")
  const[userFirstName, setUserFirstName] = useState("")
  const[userLastName, setUserLastName] = useState("")
  const[userToken,setUserToken] = useState("")
  const[userId, setUserID] = useState()
  const[mobileNo, setMobileNo] = useState()
  const[address, setAddress] = useState("")


  useEffect(() => {

    const userData = JSON.parse(localStorage.getItem('data'))
    const Fname = JSON.parse(localStorage.getItem('firstName'))
    const Lname = JSON.parse(localStorage.getItem('lastName'))

      setUserID(userData.id)
      setUserToken(userData.token)
      setUserEmail(userData.email)
      setUserFirstName(Fname)
      setUserLastName(Lname)
      setMobileNo(userData.mobileNo)
      setAddress(userData.address)

},[]);



const getUserDetails = async ()=>{
  const res = await fetch(`https://nodeserver.mydevfactory.com:3311/user/signedUserDetails?id=${userId}`,{
    method: 'GET',
    headers:{
      "Content-Type":"application/json",
      "token" : userToken
    }
  })

  const res2 = await res.json()

  if(res2.error){
    console.log(res2.message)
  }

  else{
    console.log("response get user ",res2)
      setUserEmail(res2.data.email)
      console.log(res2.data.email)
      setMobileNo(res2.data.mobile_no)
      setAddress(res2.data.address)

      localStorage.setItem('firstName', JSON.stringify(res2.data.first_name))
      localStorage.setItem('lastName', JSON.stringify(res2.data.last_name))
      location.reload()

   
  }


}


const updateDetails = async (e)=>{
  e.preventDefault()

  const res = await fetch(`https://nodeserver.mydevfactory.com:3311/user/updateProfile`,{
    method: 'POST',
    headers:{
      "Content-Type":"application/json",
      "token" : userToken
    },
    body:JSON.stringify({
      "user_id": userId,
      "first_name": userFirstName,
      "last_name": userLastName,
      "mobile_no": mobileNo,
      "address": address
    })
  })

  const res2 = await res.json()
  if(res2.error){
    console.log(res2.message)
  }
  else{
    getUserDetails()
  }
  
}

  return (
      <div className="innercontent_outer">
        <div className="container">
          <div className="row">
              <div className="col-sm-12">
                  <div className="inner_main_content">
                      <div className={styles.myaccount_info}>
                        <div className={styles.addprofile_add}>
                            <div className={styles.profile_pic}>
                                <img src="/assets/images/myaccount/profile.png" alt=""/>
                                <span><i className="bi bi-camera"></i></span>
                            </div>
                        </div>
                        <div className={styles.inner_cont_text}>
                          <h2>My Account</h2>
                          <p>You can view and edit and add your information (You can not chage your Login Email)</p>
                          <div className="login_opt_details">
                              <div className="inp_row loginmail_option">
                                <div className="form-floating withicon">
                                      <i className="bi bi-lock"></i>
                                      <input type="email" className="form-control" id="loginmail" placeholder="name@example.com" name="loginmail" value={userEmail} />
                                      <label htmlFor="loginmail">login Mail</label>
                                </div>
                              </div>
                          </div>
                          <h3>Edit Profile details</h3>
                          <p>Quisque nec lectus purus. Curabitur enim nisi</p>
                          <form onSubmit={(e)=>updateDetails(e)}>
                          <div className="myaccountdetails">
                              <div className="inp_row">
                                  <div className="form-floating">
                                    <input type="text" className="form-control" id="firstName" value={userFirstName} onChange={(e)=>setUserFirstName(e.target.value)} name="firstname" />
                                    <label htmlFor="fname">FIrst Name</label>
                                  </div>
                                  <div className="form-floating">
                                    <input type="text" className="form-control" id="lastName" value={userLastName} onChange={(e)=>setUserLastName(e.target.value)} name="lastname" />
                                    <label htmlFor="lname">Last Name</label>
                                  </div>
                                  <div className="form-floating">
                                    <input type="text" className="form-control" id="mobileNo" value={mobileNo} onChange={(e)=>setMobileNo(e.target.value)} name="name" />
                                    <label htmlFor="mobileNo">Mobile No</label>
                                  </div>
                                  <div className="form-floating">
                                    <input type="text" className="form-control" id="lastName" value={address} onChange={(e)=>setAddress(e.target.value)} name="address" />
                                    <label htmlFor="address">Address</label>
                                  </div>
                              </div>

                              <div className="inp_from_row">
                                {/* <button type="button" className="btn_opt gray_btn" onClick = {Header.showModalresetpasword}>Chage Password ? </button> */}
                                <button type="submit" className="btn_opt Subscribe_btn">Update</button>

                            </div>

                            

                          </div>
                          </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    );
};

export default MyAccountPage;
