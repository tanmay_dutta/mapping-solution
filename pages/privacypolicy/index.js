import styles from '../../styles/testimonails.module.css';
import Head from "next/head";
import Link from "next/link";

import React from 'react'


const PrivacyPolicy = (props) => {
  return (
    <div className="innercontent_outer">
      <div className="container"> 
        <div className="row">
            <div className="col-sm-12">
                <div className="inner_maincontent_view inner_cont_text ">

            
                    <div className={styles.contactus_banner}>
                          <h2>Privacy Policy</h2>
                          <p>Our collection sets standards in terms of exclusivity and style of exquisite carpets</p>
                      </div>


                <h3>Privacy policy</h3>
                <ul>
                    <li>A minimum non-refundable and non-transferable deposit is required to reserve your party date and time. Sales tax is added to every party invoice.</li>
                    <li>A non-refundable and non-transferable deposit is required to reserve ultimate party packages. The money goes toward your total party price.</li>
                    <li>A $150 fee is automatically charged if you cancel or reschedule a party within 3 weeks of your party date. </li>
                    <li>If you must cancel your party within 7 days of your scheduled party date or you do not show up to your party, you will be charged for the entire party cost, regardless of reason.</li>
                    <li>All prices are subject to change without prior notice.</li>
                </ul>
                <h3>Heading one</h3>
                <ul>
                    <li>As a courtesy to our open play patrons, no outside birthday parties or celebrations are allowed on weekdays or during reserved private play time.</li>
                    <li>Decorations, party supplies, desserts and the singing of “Happy Birthday” are absolutely not allowed at Wonderwild on weekdays or reserved private play time.</li>
                    <li>Check out <a href=""><strong>Important Details</strong></a> You Should Know to see the latest and full party policies</li>
                </ul>
                <h3>Heading Two</h3>
                <ul>
                    <li>Safety is our number one concern. Wonderwild is a parent-supervised facility. Children must be supervised at all times.</li>
                    <li>All guests are required to sign a waiver before entering the play area.</li>
                    <li>All adults/children must wear socks or closed-toe shoes as appropriate in the play area and adhere to all rules posted.</li>
                    <li>Staff members are available to assist you, however, they should not be a substitute for the parent or adult caregiver.</li>
                    <li>Adults must enter and leave with the children that they have brought into the facility.</li>
                    <li>Any inappropriate or unsafe behavior, item or equipment should be brought to the staff’s attention immediately. </li>
                    <li>Parents or guardians must refrain from physically disciplining or yelling at their child at Wonderwild such as to disturb other guests.  </li>
                      <li>Any children sick or exhibiting signs of sickness should leave immediately and join us another day when they are feeling better. All children must wear diapers or be fully potty-trained to play on the equipment.</li>
                </ul>
                <h3>Heading Two</h3>
                <ul>
                    <li>Safety is our number one concern. Wonderwild is a parent-supervised facility. Children must be supervised at all times.</li>
                    <li>All guests are required to sign a waiver before entering the play area.</li>
                    <li>All adults/children must wear socks or closed-toe shoes as appropriate in the play area and adhere to all rules posted.</li>
                    <li>Staff members are available to assist you, however, they should not be a substitute for the parent or adult caregiver.</li>
                    <li>Adults must enter and leave with the children that they have brought into the facility.</li>
                    <li>Any inappropriate or unsafe behavior, item or equipment should be brought to the staff’s attention immediately. </li>
                    <li>Parents or guardians must refrain from physically disciplining or yelling at their child at Wonderwild such as to disturb other guests.  </li>
                      <li>Any children sick or exhibiting signs of sickness should leave immediately and join us another day when they are feeling better. All children must wear diapers or be fully potty-trained to play on the equipment.</li>
                </ul>

                <h3>OTHER</h3>
                <ul>
                    <li>We allow outside food and drinks in party rooms during open play, excluding Friday parties. Breakable containers are not allowed.</li>
                    <li>Wonderwild does not claim to be an allergy-free (peanut, tree nut, etc) facility. Please seriously consider this before attending Wonderwild with sensitive children.</li>
                    <li>During a state wide mask mandate or if CDC recommends, masks are required for anyone 10 and older. Masks are encouraged at all other times.</li>
                    <li>Socks are required for all children and adults playing at Wonderwild.</li>
                    <li>Older children playing too rough in Wonderwild, as deemed by the staff, will be asked to move to the Wonderwild(er)side, or to leave. No refunds will be given.</li>
                    <li>No refunds are provided for birthday party deposits, open play visits, ten passes, memberships or sale items.</li>
                </ul>

              </div>

            </div>
        </div>
      </div>
    </div>
  );
};    

export default PrivacyPolicy;
