import React from 'react'
import styles from '../../styles/testimonails.module.css';
import Head from "next/head";
import Link from "next/link";

const TermsConditions = (props) => {
  return (

<div className="innercontent_outer">
<div className="container"> 
  <div className="row">
      <div className="col-sm-12">
          <div className="inner_maincontent_view inner_cont_text ">

      
              <div className={styles.contactus_banner}>
                    <h2>Terms & Conditions</h2>
                    <p>Our collection sets standards in terms of exclusivity and style of exquisite carpets</p>
                </div>


          <h3>Terms & Conditions</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>


          <ul>
              <li>A minimum non-refundable and non-transferable deposit is required to reserve your party date and time. Sales tax is added to every party invoice.</li>
              <li>A non-refundable and non-transferable deposit is required to reserve ultimate party packages. The money goes toward your total party price.</li>
              <li>A $150 fee is automatically charged if you cancel or reschedule a party within 3 weeks of your party date. </li>
              <li>If you must cancel your party within 7 days of your scheduled party date or you do not show up to your party, you will be charged for the entire party cost, regardless of reason.</li>
              <li>All prices are subject to change without prior notice.</li>
          </ul>
         
    
        

          <h3>OTHER</h3>
          <ul>
              <li>We allow outside food and drinks in party rooms during open play, excluding Friday parties. Breakable containers are not allowed.</li>
              <li>Wonderwild does not claim to be an allergy-free (peanut, tree nut, etc) facility. Please seriously consider this before attending Wonderwild with sensitive children.</li>
              <li>During a state wide mask mandate or if CDC recommends, masks are required for anyone 10 and older. Masks are encouraged at all other times.</li>
              <li>Socks are required for all children and adults playing at Wonderwild.</li>
              <li>Older children playing too rough in Wonderwild, as deemed by the staff, will be asked to move to the Wonderwild(er)side, or to leave. No refunds will be given.</li>
              <li>No refunds are provided for birthday party deposits, open play visits, ten passes, memberships or sale items.</li>
          </ul>

        </div>

      </div>
  </div>
</div>
</div>
  );
};    

export default TermsConditions;
